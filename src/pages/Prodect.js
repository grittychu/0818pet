import React from "react";
import { useEffect, useState } from "react";
import axios from "axios";
import "../Prodect.css";
import ProdectItem from "./ProdectItem.js";
import { useNavigate } from "react-router-dom";
import Modal from "./prodectItemDetailModal";

const Prodect = () => {
  const [prodect, setProdect] = useState([]);
  const [searchProdect, setSearchProdect] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const Navigate = useNavigate();
  const [detailModal, setDetailModal] = useState(false);
  const [currentSelect, setCurrentSelect] = useState({});

  useEffect(() => {
    axios.get("http://localhost:4000/menu").then((res) => {
      setProdect(res.data);
      setSearchProdect(res.data);
    });
  }, []);

  const handleChange = (e) => {
    if (e.key === "Enter") {
      console.log(e.target.value);

      if (e.target.value === "") {
        setSearchProdect(prodect);
        return;
      }

      let tempList = [];

      for (let i = 0; i < prodect.length; i++) {
        if (prodect[i].place === e.target.value) {
          tempList.push(prodect[i]);
        }
      }
      setSearchProdect(tempList);
    }
    // Navigate(`/search?q=${e.target.value}`);
  };

  const parentsFunction = (e) => {
    setDetailModal(true);
    setCurrentSelect(e);

    console.log(e);
  };

  return (
    <>
      <header className="header">
        <div>
          <p className="searchText">
            당신을 기다리고 있는
            <br />
            새가족이 있습니다.
            <br />
            따뜻한 마음을 기다리고 있습니다.
          </p>

          <input
            onKeyDown={handleChange}
            className="searchInput"
            type="text"
            placeholder="지역을 입력해주세요."
          />
        </div>
      </header>
      <div className="formProdect">
        <h1 style={{ padding: "15%", textAlign: "center" }}>
          PETMILY
          <br />
          INTRODUCE
        </h1>
        {searchProdect.map((item, idx) => (
          <ProdectItem prodect={item} key={idx} parentsFunc={parentsFunction} />
        ))}

        {detailModal && (
          <Modal
            desc={currentSelect}
            closeModal={() => setDetailModal(!detailModal)}
          />
        )}
      </div>
    </>
  );
};

export default Prodect;
