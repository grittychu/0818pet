import React from 'react';
import { useEffect, useState } from 'react';
import axios from "axios";
import ProdectItem from "./ProdectItem.js";
import '../myPage.css';
import UserItem from '../component/components/UserItem';

function MyPage({ userId }) {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);
  const [prodect, setProdect] = useState([]);
  
  useEffect(() => {
    window
      .fetch(`http://localhost:4000/form`)
      .then((res) => res.json())
      .then((user) => {
        setUser(user);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }, [userId]);

  useEffect(() => {
    axios.get("http://localhost:4000/menu").then((res) => {
      let favoriteId = JSON.parse(localStorage.getItem("favorite"));
      console.log(res.data);

      let favoriteRes = [];
      for (let i=0; i < res.data.length ; i++) {
        for (let j=0; j < favoriteId.length; j++) {
          if (res.data[i].id == favoriteId[j]) {
            console.log(res.data[i]);
            favoriteRes.push(res.data[i]);
            break;
          }
        }
      }
      setProdect(favoriteRes);
    });
  }, []);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;
  return (
    <>
      <header className="header">Login</header>
      <div className="mypage">
        <div className="list">
          <h1>신청 조회</h1>
        </div>
        <div>
        { user.map(user => <UserItem key={ user.userName } user={ user }/>) }
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <div className="wishlist">
          <h1 style={{ textAlign: "center" }}>찜 목록</h1>
          {prodect.map((item, idx) => (
            <ProdectItem prodect={item} key={idx}/>
          ))}
        </div>
      </div>
    </>
  );
}
export default MyPage;