import React from "react";
import { Link } from 'react-router-dom';
import { useEffect, useState } from "react";
 
function Modal(props) {
  const [favorite, setFavorite] = useState(() => {
    if (typeof window !== "undefined") {
      const saved = window.localStorage.getItem("favorite");
      if (saved !== null) {
        return JSON.parse(saved);
      } else {
        return [];
      }
    }
  });
  const [imgStatus, setImgStatus] = useState('/favorite_n.png');

  useEffect(() => {
    localStorage.setItem("favorite", JSON.stringify(favorite));

    var arr = localStorage.getItem("favorite");
    arr = JSON.parse(arr);
    arr = new Set(arr);
    arr = [...arr];
    localStorage.setItem("favorite", JSON.stringify(arr));

  }, [favorite]);

  function closeModal() {
    props.closeModal();
  }

  const doFavorite = (e) => {
    e.preventDefault();
    setFavorite([...favorite, props.desc.id]);
    setImgStatus('/favorite_y.png')
  }
  
  return (
    <div className="Modal" onClick={closeModal}>
      <div className="modalBody" onClick={(e) => e.stopPropagation()}>
        <button id="modalCloseBtn" onClick={closeModal}>✖</button>
        <h1>{props.desc.Name} 소개합니다.</h1>
        <div className="imgDiv">
          <img
            src={props.desc.detail.image}
            style={{ maxWidth: 300 }}
            alt={props.desc.menuName}
          />
          <button className="imgBtn" onClick={doFavorite}>
            <img src={imgStatus} class="btnImages"/>
          </button>
        </div>
        <h3 className="inText">이름 : {props.desc.Name}</h3>
        <h3 className="inText">성별 : {props.desc.gender}</h3>
        <h3 className="inText">발견장소 : {props.desc.place}</h3>
        <h3 className="inText">특징 및 성격 : {props.desc.ref}</h3>
        <Link to={'../AppForm'}><button className="submitButton">입양 신청서 작성하러 가기</button></Link>
      </div>
    </div>
  );
}
 
export default Modal;
