const UserItem = ({ user }) => {
    return (
        <div className="item">
        <table>
            <thead>
                <th>이름</th>
                <th>생년월일</th>
                <th>전화번호</th>
                <th>주거형태</th>
                <th>반려견을 가지고 있나요?</th>
            </thead>
            <tbody>
                <td>{user.userName}</td>
                <td>{user.userBirth}</td>
                <td>{user.userPhone}</td>
                <td>{user.house}</td>
                <td>{user.havePets}</td>
            </tbody>
        </table>
        </div>
    );
  };
  export default UserItem;